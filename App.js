/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {View} from 'react-native';
import { Provider } from 'react-redux';
import store from './src/lib/createStore';

import Entypo from 'react-native-vector-icons/Entypo';

//screens
import Home from './src/Home';
import Search from './src/Search';
import LoginScreen from './src/Login';
import Register from './src/Register';
import Profile from './src/Profile';
import TimeLine from './src/TimeLine';
import Chat from './src/Chat';
import MapScreen from './src/MapScreen';
import Forgot from './src/Login/Forgot';
//navigation
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();


//sql


const Blank = () => {
  return <View />;
};
const HomePage = () => {
  const screenOptions = ({route}) => ({
    tabBarIcon: ({focused, color, size}) => {
      let iconName;

      if (route.name === 'Explore') {
        iconName = 'home';
      } else if (route.name === 'Watchlist') {
        iconName = 'heart';
      } else if (route.name === 'Messengers') {
        iconName = 'price-tag';
      } else if (route.name === 'Notification') {
        iconName = 'bell';
      } else if (route.name === 'Profile') {
        iconName = 'user';
      } else if (route.name === 'Maps') {
          iconName = 'medium-with-circle';
      }

      // You can return any component that you like here!
      return <Entypo name={iconName} size={size} color={color} />;
    },
  });
  return (
    <Tab.Navigator
      screenOptions={screenOptions}
      tabBarOptions={{
        activeTintColor: '#1F37FB',
        inactiveTintColor: '#464962',
      }}>
      <Tab.Screen name="Explore" component={Home} />
      <Tab.Screen name="Watchlist" component={Search} />
      <Tab.Screen name="Messengers" component={Chat} />
      <Tab.Screen name="Notification" component={TimeLine} />
      <Tab.Screen name="Profile" component={Profile} />
      <Tab.Screen name="Maps" component={MapScreen} />
    </Tab.Navigator>
  );
};

const App = () => {
    let content = (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}} />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name='Forgot' component={Forgot} />
                <Stack.Screen
                    name="HomePage"
                    component={HomePage}
                    options={{headerShown: false}}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
  return (
      <Provider store={store}>
          {content}
      </Provider>
  );
};
export default App;
