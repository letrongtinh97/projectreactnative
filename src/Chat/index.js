import React, { Component } from 'react';
import {
    View,
    Platform,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';
import {useNavigation} from '@react-navigation/native'
import Toolbar from '../components/Toolbar';
import InputModule from '../components/InputModule';
import KeyboardSpacer from '../components/KeyboardSpacer';
const Chat = () => {
    const navigation = useNavigation();

    const onBackPress = () => {
        return navigation.navigate('HomePage');
    }
    const dismissKeyboard = () => {
        Keyboard.dismiss();
    };
    return (
        <View style={{ flex: 1 }}>
            <Toolbar onBackPress={() => console.log('onBackPress')} />
            <TouchableWithoutFeedback onPress={dismissKeyboard}>
                <View style={{ flex: 1 }} />
            </TouchableWithoutFeedback>
            <InputModule />
            {Platform.OS === 'ios' && <KeyboardSpacer />}
        </View>
    );
}
export default Chat;
