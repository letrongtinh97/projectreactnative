var CryptoJS = require("crypto-js");
const SECRET_KEY = 'test';

export const encrypt = (text) => {
    let ciphertext = CryptoJS.AES.encrypt(text, SECRET_KEY).toString();
    return ciphertext;
}

export const decrypt = (text) => {
    const bytes  = CryptoJS.AES.decrypt(text, SECRET_KEY);
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
}
