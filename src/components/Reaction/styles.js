import {StyleSheet} from 'react-native';
import metrics from '../../config/metrics';

const styles = StyleSheet.create({
    card: {
        borderRadius: 30,
        width:200
    },
    list: {
        overflow: 'visible',
    },
    reactView: {
        width: (metrics.screenWidth - 24) / 6,
        height: 58,
        justifyContent: 'center',
        alignItems: 'center',
    },
    reaction: {
        width: 40,
        height: 40,
        marginBottom: 0,
    },
});

export default styles;
