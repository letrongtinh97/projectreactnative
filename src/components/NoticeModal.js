import React, {Component} from 'react';
import {View, Text,Dimensions,StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import Spinner from 'react-native-spinkit'
const NoticeModal = (props) => {
    const {show, text} = props
    return(
        <View style={styles.container}>
            <Modal isVisible={show ? show : false}>
                <View style={{ flexDirection:'column',alignItems: 'center', justifyContent: 'center' }}>
                    <Spinner
                        style={styles.spinner}
                        isVisible={true}
                        size={100}
                        type={'9CubeGrid'}
                        color={'#4267b2'}
                    />
                    <Text style={{color:'red'}}>{text}</Text>
                </View>
            </Modal>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.1)',
    },

    spinner: {
        marginTop:10
    },


});
export default NoticeModal;
