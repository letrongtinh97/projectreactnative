import React,{useEffect,useState} from 'react';
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    Alert,
} from 'react-native';

import {LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager} from 'react-native-fbsdk';
import Input from './Input';
import Button from './Button';
import {useNavigation, useRoute} from '@react-navigation/native';
import { connect, useSelector, useDispatch } from 'react-redux'
import style from './styles';
import {login,loginNormal} from '../redux/actions/login'
import NoticeModal from '../components/NoticeModal';
import Modal from 'react-native-modal';
const SQLite = require('react-native-sqlite-storage')
const db = SQLite.openDatabase({name: 'test.db', createFromLocation: '~sqlliteDB.db'})
const LoginScreen = (props) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const route = useRoute();
    const [loadHome, setLoadHome] = useState(false);
    const [gif, setGif] = useState(false)
    const [id,setId] = useState('');
    const [password,setPassword] = useState('');
    const [error,setError] = useState('');
    const [error1,setError1] = useState('');
    const [text, setText] = useState('')
    const [status,setStatus] = useState(0)
    const [pin,setPin] = useState(false)
    const [accToken, setAccToken] = useState('')
    //console.log(navigation.)

    useEffect(()=>{
        db.transaction((tx) => {
            tx.executeSql('SELECT * FROM users ','', (tx, results) => {
                const len = results.rows.length;
                if(len > 0) {
                    // exists owner name John
                    const row = results.rows.item(0);
                    navigation.navigate('HomePage')
                   //dispatch(login(row));
                }
            });
        });
    },[])
    useEffect(()=>{
        route?.params?.id ? setId(route?.params?.id) : ''
        route?.params?.password ? setPassword(route?.params?.password) : ''
    },[route])

    useEffect(()=>{
        if(props.login.code === 0){
            setLoadHome(true) ;
            setPin(true)

        }else {
            setLoadHome(false);
            setPin(true)
        }

    },[props.login])
    useEffect(() => {
        if(pin){
            if(loadHome === true){
                setGif(false)
                return navigation.navigate('HomePage')
            }
            props?.login?.status ? setStatus(props.login.status) : setStatus(0)
            setError1('Mật khẩu không chính xác')

        }
        setGif(true)
        setTimeout(() => setGif(false), 3000)
        setPin(false)
    },[pin])


    const loginWithFacebook = () => {
        const get_Response_Info = (error, result) => {
            console.log('result',result)
            if (error) {
                Alert.alert('Error fetching data: ' + error.toString());
            } else {
                dispatch(login(result));
            }
        };
        const get_Response_Friend = (error, result) => {
            console.log('get_Response_Friend',result)
            if (error) {
                Alert.alert('Error fetching data: ' + error.toString());
            } else {
               console.log('err')
            }
        };

        LoginManager.logInWithPermissions(['public_profile', 'email','user_friends']).then(
            function (result) {
                if (result.isCancelled) {
                    console.log('==> Login cancelled');
                } else {
                    console.log(
                        '==> Login success with permissions: ' +
                        result.grantedPermissions.toString(),
                    );
                    AccessToken.getCurrentAccessToken().then((data) => {
                        console.log(data.accessToken.toString());
                        setAccToken(data.accessToken.toString());
                    });
                    const processRequest = new GraphRequest(
                        '/me?fields=name,picture.type(large)',
                        null,
                        get_Response_Info,
                    );

                    // Start the graph request.
                    new GraphRequestManager().addRequest(processRequest).start();
                    setText('Loading ....')
                    setGif(true)
                }
            },
            function (error) {
                console.log('==> Login fail with error: ' + error);
            },
        );
    };

    const handleSignIn = () => {
        if(id.length < 6 || password.length < 6) {
            return setError('Tài khoản dưới 6 kí tự')
        }

        let rqData = {
            username: id,
            password,
            login: 1
        }

        dispatch(login(rqData));
    }
    return (
        <SafeAreaView style={style.container}>

            <View style={style.wrapper}>
                <Text style={style.header}>Sign In</Text>
                <View>
                    <Input
                        onChangeText={text => setId(text)}
                        value={id}
                        placeholder={'ID'}
                    />
                    {
                        id.length < 6 ?  <Text style={{color:'red', paddingLeft:10}}>{error}</Text> : null
                    }


                    <Input
                        onChangeText={text => setPassword(text)}
                        value={password}
                        placeholder={'Password'} secureTextEntry
                    />
                    {
                        status ===20001 ?  <Text style={{color:'red', paddingLeft:10}}>{error1}</Text> : null
                    }
                    {
                        password.length < 6 ?  <Text style={{color:'red', paddingLeft:10}}>{error}</Text> : null
                    }

                </View>
                <View style={style.forgotContainer}>
                    <TouchableOpacity onPress={()=>navigation.navigate('Forgot')}>
                        <Text style={style.btnTextForgot}>Forgot Password ?</Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity
                    onPress={()=> handleSignIn()}
                    style={style.btnLogin}>
                    <Text style={[style.btnTextForgot, {color: 'white'}]}>Sign In</Text>
                </TouchableOpacity>
                <Text style={{textAlign: 'center', padding: 20}}>or</Text>
                <View style={style.ggBtn}>
                    <Button title="Google" disable={true} icon={require('../assets/googleIcon.png')}/>
                    <Button
                        onPress={() => loginWithFacebook()}
                        title="Facebook"
                        icon={require('../assets/fbIcon.png')}
                        color={'#4a6ea8'}
                        textColor="#fff"
                    />
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        padding: 10,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 20,
                    }}>
                    <Text>Not yet a member,</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text style={[style.btnTextForgot, {color: 'red'}]}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
                <NoticeModal show={gif} text={text} />
            </View>
        </SafeAreaView>
    );
};
const mapStateToProps = state => {
    return {
        login: state.login,
    }
}

export default connect(mapStateToProps)(LoginScreen);
