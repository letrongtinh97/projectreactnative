import React,{useEffect,useState} from 'react';
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
} from 'react-native';

import Input from './Input';
import {useNavigation, useRoute} from '@react-navigation/native';
import { connect, useDispatch } from 'react-redux'
import style from './styles';
import NoticeModal from '../components/NoticeModal';
const Forgot = (props) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const route = useRoute();
    const [loadHome, setLoadHome] = useState(false);
    const [gif, setGif] = useState(false)
    const [id,setId] = useState('');
    const [password,setPassword] = useState('');
    const [error,setError] = useState('');
    const [error1,setError1] = useState('');
    const [text, setText] = useState('')
    const [status,setStatus] = useState(0)
    const [pin,setPin] = useState(false)
    //console.log(navigation.)


    useEffect(()=>{

    },[route])

    useEffect(()=>{


    },[props.login])

    useEffect(() => {

    },[pin])




    const handleChangePassword = () => {

    }
    return (
        <SafeAreaView style={style.container}>

            <View style={style.wrapper}>
                <Text style={style.header}>Forgot</Text>
                <View>
                    <Input
                        onChangeText={text => setId(text)}
                        value={id}
                        placeholder={'ID'}
                    />
                    {
                        id.length < 6 ?  <Text style={{color:'red', paddingLeft:10}}>{error}</Text> : null
                    }
                    <Input
                        onChangeText={text => setId(text)}
                        value={id}
                        placeholder={'Old Password'}
                    />
                    {
                        id.length < 6 ?  <Text style={{color:'red', paddingLeft:10}}>{error}</Text> : null
                    }


                    <Input
                        onChangeText={text => setPassword(text)}
                        value={password}
                        placeholder={'New Password'} secureTextEntry
                    />
                    {
                        status ===20001 ?  <Text style={{color:'red', paddingLeft:10}}>{error1}</Text> : null
                    }
                    {
                        password.length < 6 ?  <Text style={{color:'red', paddingLeft:10}}>{error}</Text> : null
                    }

                </View>


                <TouchableOpacity
                    onPress={()=> handleChangePassword()}
                    style={style.btnLogin}>
                    <Text style={[style.btnTextForgot, {color: 'white'}]}>Forgot</Text>
                </TouchableOpacity>

                <NoticeModal show={gif} text={text} />
            </View>
        </SafeAreaView>
    );
};
const mapStateToProps = state => {
    return {
        login: state.login,
    }
}

export default connect(mapStateToProps)(Forgot);
