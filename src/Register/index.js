import React, {useState, useEffect} from 'react';
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import axios from 'axios';
import Input from './Input';
import Button from './Button';
import {useNavigation} from '@react-navigation/native';
import {encrypt} from '../lib/crypto'
import style from './styles';
import { API_KEY, BASE_URL } from 'react-native-dotenv'
import {login} from '../redux/actions/login';
const apiKey = API_KEY;
const url = BASE_URL;
const SQLite = require('react-native-sqlite-storage')
const db = SQLite.openDatabase({name: 'test.db', createFromLocation: '~sqlliteDB.db'})
const Register = () => {
    const navigation = useNavigation();
    const [name,setName] = useState('');
    const [username,setUserName] = useState('')
    const [password, setPassword] = useState('')
    const [cfpassword, setCfPassword] = useState('')
    const [error,setError] = useState(false);
    const [errorPass,setErrorPass] = useState(false)
    const [notice,setNotice] = useState('');
    const handleCreateUser = () => {
        if(!name || !username || !password || !cfpassword){
            setNotice('*Vui long nhap cac o')
            return setError(true)
        }
        if(password !== cfpassword){
            return setErrorPass(true)
        }
        const en_password = encrypt(password)
        let data = {
            name: name,
            username: username,
            password: en_password
        }
        axios.post(`${url}user/create`,data)
            .then((rs)=>{
                console.log(rs.data)
                if(rs.data.code === 0){
                    console.log('rs.data',rs.data);
                    // db.transaction((tx) => {
                    //     tx.executeSql('SELECT * FROM users ','', (tx, results) => {
                    //         const len = results.rows.length;
                    //         if(len > 0) {
                    //             // exists owner name John
                    //             const row = results.rows.item(0);
                    //             dispatch(login(row));
                    //         }
                    //     });
                    // });
                    return navigation.navigate('Login',{id:username, password: password})
                }
                if(rs.data.code === 2){
                    setError(true)
                    return setNotice('*Tai khoan da ton tai');
                }
            })
            .catch((e)=>{
                setUserName('')
                setCfPassword('')
                setPassword('')
                setName('')
            });
    }
    return (
        <SafeAreaView style={style.container}>
            <ScrollView style={{flexDirection: 'column'}}>

                <View style={style.wrapper}>
                    <Text style={style.header}>Register</Text>
                    {
                        error ? <Text style={{color:'red', paddingLeft:10}}>{notice}</Text> : null
                    }
                    <View>
                        <Input value={name} onChangeText={(text)=>setName(text)} placeholder={'Full Name'}/>
                        <Input value={username} onChangeText={(text)=>setUserName(text)} placeholder={'ID'}/>
                        <Input value={password} onChangeText={(text)=>setPassword(text)} placeholder={'Password'} secureTextEntry/>
                        <Input value={cfpassword} onChangeText={(text)=>setCfPassword(text)} placeholder={'Confirm Password'} secureTextEntry/>
                    </View>
                    <View style={style.wrapperRegister}>
                        <TouchableOpacity
                            onPress={() => handleCreateUser()}
                            style={style.btnLogin}>
                            <Text style={[style.btnTextForgot, {color: 'white'}]}>Register</Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            padding: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 20,
                        }}>
                        <Text>You are a member,</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                            <Text style={[style.btnTextForgot, {color: 'red'}]}>Sign In</Text>
                        </TouchableOpacity>
                    </View>


                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default Register;
