import {put,takeLatest,all,call} from "redux-saga/effects"
import {FETCH, DATA, SUCCESS, FAIL} from '../actions/types/login';
import {saveFB,getUserNormal} from '../api/login';

function* getDataLogin(action) {
    console.log('action', action)
    try {
        let dataLogin = ''
        if(action.data.login){
            dataLogin = yield call(getUserNormal,action.data)
            yield put({type:SUCCESS,dataLogin})
        }else {
            dataLogin = yield call(saveFB,action.data)
            yield put({type:SUCCESS,dataLogin})
        }

    }catch (e) {
        yield put({type:FAIL,e})
    }
}

export function* watchLogin() {
    yield takeLatest(FETCH,getDataLogin)
}

