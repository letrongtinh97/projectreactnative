import {put,call,takeLatest,fork} from "redux-saga/effects"
import {FETCH,FAIL,SUCCESS} from '../actions/types/home';
import {test} from '../api/home';

function* getDataHome(action) {
    try {
        const getData = yield call(test);
        yield put({type:SUCCESS,getData})

    }catch (e) {
        yield put({type: FAIL}, e)
    }

}

export function* watchHome() {
    yield takeLatest(FETCH,getDataHome)
}

