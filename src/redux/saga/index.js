import { all } from 'redux-saga/effects';
import {watchHome} from './home';
import {watchLogin} from './login';
const root = function* root() {
    yield all([
        watchHome(),
        watchLogin(),
    ]);
}

export default root;
