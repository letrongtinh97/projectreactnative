import axios from 'axios';
import { API_KEY, BASE_URL } from 'react-native-dotenv'
const token = API_KEY;
const url = BASE_URL;
const config = async () => {
    return {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
    };
};

export const saveFB = (action) => {
    return axios.post(`${url}user/user-facebook`, action);
};

export const getUserNormal = (action) => {
    return axios.post(`${url}user/get-user`,action)
}

