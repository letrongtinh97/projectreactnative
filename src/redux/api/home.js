import axios from 'axios';
const token = '123';
const config = async() => {
    return {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    }
}

export const test = async(action) =>{
    return axios.post(`timeline/write-image`,action);
};

