import {FETCH,FAIL,SUCCESS} from '../actions/types/home';
const initStates = {
    loading: true
}

function home(state = initStates , action) {
    switch (action.type) {
        case FETCH:
            return {
                ...state,
            }
        case SUCCESS:
            return {
                ...state,
                loading: false
            }
        case FAIL:
            return {
                ...state
            }
        default:
            return state
    }
}
export default home;
