import {FETCH,DATA,SUCCESS,FAIL} from '../actions/types/login';
const initStates = {
    loading: true,
    user: '',
    status: '',
    code: 1
}

function login(state = initStates , action) {

    switch (action.type) {
        case FETCH:
            return {
                ...state
            }
        case DATA:
            return state
        case SUCCESS:
            console.log('action?.dataLogin?.data',action?.dataLogin?.data)
            if(action?.dataLogin?.data?.code === 20001){
                return {
                    ...state,
                    status: 20001
                }
            }
            let dataResult = action?.dataLogin?.data ? action?.dataLogin?.data : ''
            let dataStatus =  action?.dataLogin?.status ? action?.dataLogin?.status : 0
            return {
                ...state,
                loading: false,
                user: dataResult,
                status: dataStatus,
                code: 0
            }
        case FAIL:
            return {
                ...state,
            }
        default:
            return state
    }
}
export default login;
