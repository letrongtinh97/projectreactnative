import React from 'react';
import {
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ListCard from './ListCard';
import BackgroundCurve from '../components/BackgroundCurve';
import {useNavigation} from '@react-navigation/native';
const Home = () => {
  const navigate = useNavigation();

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <BackgroundCurve style={styles.svg} />
        <ScrollView style={styles.scrollView}>
          <View style={styles.headerContainer}>
            <View style={styles.headerGroupIndicator}>
              <View style={styles.headerGroupIndicatorLeft}>
                <Feather name="map-pin" color="#fff" size={16} />
                <Text style={styles.headerGroupIndicatorText}>
                  Boston (BOS)
                </Text>
              </View>
              <View style={styles.headerGroupIndicatorRight}>
                <Feather name="settings" color="#fff" size={16} />
              </View>
            </View>
            <Text style={styles.heading}>
              {'Where would \nyou want to go?'}
            </Text>
            <View style={styles.groupInputContainer}>
              <View style={styles.inputSearchContainer}>
                <TextInput
                  style={styles.inputSearch}
                  value="New York Citi (JFK) "
                />
                <TouchableOpacity
                  style={styles.buttonSearch}
                  onPress={() => navigate.navigate('Watchlist')}>
                  <Feather name="search" color="gray" size={16} />
                </TouchableOpacity>
              </View>
              <View style={styles.listBtn}>
                <TouchableOpacity style={styles.button}>
                  <Ionicons name="ios-airplane" color="#fff" size={16} />
                  <Text style={styles.buttonText}>Flights</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.button, {backgroundColor: 'transparent'}]}>
                  <FontAwesome name="hotel" color="#fff" size={16} />
                  <Text style={styles.buttonText}>Hotels</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <ListCard />
        </ScrollView>
      </View>
    </>
  );
};

export default Home;
