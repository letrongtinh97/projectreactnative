import React, {useEffect, useState, useRef} from 'react';
import { Text, View, TextInput,ScrollView,PermissionsAndroid, TouchableOpacity} from 'react-native';
import { BASE_URL_GLITCH, API_KEY} from 'react-native-dotenv'
import axios from 'axios'
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FAIcon from "react-native-vector-icons/FontAwesome";
import MapView, {Marker} from 'react-native-maps';
import LinearGradient from 'react-native-linear-gradient';
import {CardHome} from './HomeScreen';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import RBSheet from "react-native-raw-bottom-sheet";
import styles from './styles'
import _ from "lodash";
FAIcon.loadFont();

const Header = () => {
    const [textAddress, setTextAddress] = useState('')
    const [debounce, setDebounce] = useState(false)
    const [name, setName] = useState('')
    const [coord,setCoord] = useState({})
    const handleFindAdress = (name) => {
        setDebounce(true)
        clearTimeout(timeoutCheck);
        let timeoutCheck = setTimeout(() => {
            let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${name}&key=${API_KEY}`
            let data = axios.get(url).then((rs)=> {
                return rs.json();
                setDebounce(false)
            }).then((rss)=>{
              console.log('rss',rss)
            }).catch((e)=>{
                setDebounce(false)
            })
            setDebounce(false)
        }, 1000);
    }
    return (
        <View style={styles.header}>

            <View style={{paddingTop:50}}>
                <View style={styles.wrapperInput}>
                    <TouchableOpacity disabled={debounce}  onPress={()=>handleFindAdress(textAddress)}>
                        <AntDesign name="search1" size={18} color="gray" />
                    </TouchableOpacity>
                    <TextInput style={styles.inputText} value={textAddress} onChangeText={(text) => setTextAddress(text)} />
                </View>
                <View style={styles.wrapperInput}>
                    <Feather name="map-pin" size={18} color="gray" />
                    <TextInput
                        style={[styles.inputText, {color: '#9770A3'}]}
                        value="Current Location"
                    />
                    <Text>12 ml</Text>
                </View>
            </View>
        </View>
    );
};

const Map = (props) => {
    const refRBSheet = useRef();
    const refAddress = useRef();
    const [lai, setLai] = useState(10.7553405)
    const [long, setLong] = useState(106.4143465)
    const [show, setShow] = useState(false)
    const [action, setAction] = useState(false)
    const [address, setAddress] = useState([])
    const [addressWork, setAddressWork] = useState('')
    const [name, setName] = useState('')
    const [coordId, setCoordId] = useState({})
    const [loa,setLoa] = useState(false)
    useEffect(()=> {
        axios.get(`${BASE_URL_GLITCH}map/get-doc`).then((rs)=> {
            rs?.data?.data ?  setAddress(rs?.data?.data) : []
            //console.log(rs)
        }).catch((e)=> setAddress([]))
    },[])

    return (
        <View>
            <RBSheet
                ref={refAddress}
                closeOnDragDown
                height={350}
                customStyles={{
                    container: {
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10
                    }
                }}
            >
                <ScrollView>
                    <View style={styles.gridContainer}>
                        {address.map((e,i)=>(
                            <TouchableOpacity key={i} style={styles.models} onPress={()=>{
                                setAction(false)
                                setAddressWork(e.note)
                                setName(e.name)
                            }}>
                                <Text >Tên người tạo:{e.name}|| Địa chỉ:{e.note}</Text>
                                <Text >Ngày tạo:{e.create_at}</Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>
            </RBSheet>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown
                height={200}
                customStyles={{
                    container: {
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10
                    }
                }}
            >
                <ScrollView>
                    <View style={styles.gridContainer}>
                        <View style={{width:'100%', alignItems:'center',justifyContent:'center'}}>
                            <TouchableOpacity style={styles.srView} onPress={()=>{
                                refRBSheet.current.close()
                                refAddress.current.open()
                            }}>
                                <View style={styles.srViewView}>
                                    <FontAwesome5 size={25} name="user-alt" color="#ED8674" />
                                    <Text style={{paddingLeft:15}}>Thêm mới</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.srView} onPress={()=>{
                                refRBSheet.current.close()
                            }}>
                                <View style={styles.srViewView}>
                                    <FontAwesome5 size={25} name="user-alt" color="#ED8674" />
                                    <Text style={{paddingLeft:15}}>Thoat</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </RBSheet>

            <MapView
                showsUserLocation={true}
                onLongPress={(e) => {
                    console.log(e.nativeEvent.coordinate)
                    refRBSheet.current.open()
                }}
                style={styles.mapview}
                initialRegion={{
                    latitude: lai,
                    longitude: long,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}>
                <Marker
                    pointerEvents='auto'
                    stopPropagation={true}
                    //onPress={(e)=>console.log(e.nativeEvent.coordinate)}
                    //onPress={() => refAddress.current.open()}
                    coordinate={{
                        latitude: lai,
                        longitude: long,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}>
                            <View
                                style={{
                                    backgroundColor: '#EDE6EA',
                                    padding: 5,
                                    borderRadius: 40,
                                }}>
                                <View
                                    style={styles.markerModal}>
                                    <LinearGradient
                                        style={styles.marker}
                                        colors={['#714B87', '#944787', '#984587']}>
                                        <FontAwesome5 name="user-alt" color="#fff" />
                                    </LinearGradient>

                                </View>
                            </View>
                    </Marker>
            </MapView>

        </View>
    );
};

const ListCard = () => {

    return (
        <ScrollView horizontal={true}>
            <CardHome
                title="Your Next Appointment"
                noHeader
                noFooter
                book
                info={{
                    name: 'Dr T Pay Dhar',
                    time: 'Sunday, May 15th at 8:00 PM',
                    address: '570 Kemmer Shores',
                    detail: 'San Francisco, CA 90293',
                    islike: true,
                    rating: 4,
                    tag: 'Wellness',
                }}
            />
            <CardHome
                title="Your Next Appointment"
                noHeader
                noFooter
                book
                info={{
                    name: 'Dr T Pay Dhar',
                    time: 'Sunday, May 15th at 8:00 PM',
                    address: '570 Kemmer Shores',
                    detail: 'San Francisco, CA 90293',
                    islike: true,
                    rating: 4,
                    tag: 'Wellness',
                }}
            />
            <CardHome
                title="Your Next Appointment"
                noHeader
                noFooter
                book
                info={{
                    name: 'Dr T Pay Dhar',
                    time: 'Sunday, May 15th at 8:00 PM',
                    address: '570 Kemmer Shores',
                    detail: 'San Francisco, CA 90293',
                    islike: true,
                    rating: 4,
                    tag: 'Wellness',
                }}
            />
        </ScrollView>
    );
};
const permissionsLocal = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the GPS");

        } else {
            console.log("Camera permission denied");
        }
    } catch (e) {
        console.log('err')
    }
}
const MapScreen = () => {
    const [state, setState] = useState(false)

    useEffect(()=>{
        permissionsLocal().then(()=>{
            setState(true)

        }).catch(()=>setState(false))
    },[])

    return (

        <ScrollView style={styles.container}>
            {
                state ?
                    <>
                    <Header />
                    <Map state={state}  />
                    <ListCard />
                    </>
                : null
            }

        </ScrollView>
    );
};

export default MapScreen;


