import {StyleSheet,Dimensions} from 'react-native'

const styles = StyleSheet.create({
    header: {
        marginTop: 38,
        padding: 15,
    },
    headerText: {
        fontSize: 32,
        fontWeight: 'bold',
        color: '#fff',
    },
    headerRightContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {marginLeft: 12, transform: [{rotate: '-90deg'}]},
    headerBody: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        // padding: 15,
    },
    wrapperInput: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingHorizontal: 10,
        borderRadius: 8,
        marginTop: 10,
    },
    inputText: {
        padding: 10,
        flex: 1,
    },
    mapview: {
        width: Dimensions.get('window').width,
        height: 300,
    },
    marker: {
        backgroundColor: '#6C4C86',
        padding: 10,

        borderRadius: 20,
    },
    maps: {
        backgroundColor: '#C49DBD',
        padding: 5,
        borderRadius: 20,

        shadowColor: '#714B87',
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 1,
        shadowRadius: 20,
    }
    ,
    models: {
        height:60,
        alignItems:'center',
        justifyContent:'center',
        width:'100%',
        borderBottomWidth:1,
        borderColor:'#ccc'
    },
    markerModal: {
        backgroundColor: '#C49DBD',
        padding: 5,
        borderRadius: 20,

        shadowColor: '#714B87',
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 1,
        shadowRadius: 20,
    },
    gridContainer: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
        padding: 10,
        marginBottom: 20
    },
    gridButtonContainer: {
        flexBasis: "25%",
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center"
    },
    gridButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center"
    },
    gridIcon: {
        fontSize: 30,
        color: "white"
    },
    gridLabel: {
        fontSize: 14,
        paddingTop: 10,
        color: "#333"
    },
    srView: {
        height:60,
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth: 1,
        borderColor: '#ccc'
    },
    srViewView: {
        width:100, flexDirection:'row', alignItems:'center'
    }
});

export default styles
