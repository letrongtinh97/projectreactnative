import React, {Component} from 'react';
import {View} from 'react-native';
import ReactionContainer from '../components/Reaction';
const TimeLine = () => {
    return(
        <View style={{paddingTop:34}}>
            <ReactionContainer />
        </View>
    );

}

export default TimeLine;
