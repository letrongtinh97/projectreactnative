import React, {useState, useEffect, createRef} from 'react';
import {Text, View, SafeAreaView, ScrollView, TouchableOpacity} from 'react-native';
import {connect, useSelector, useDispatch} from 'react-redux';
import FastImage from 'react-native-fast-image';
import ActionSheet from 'react-native-actions-sheet';
//icon
import mocks from './mocks.json';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';
import { FAB } from 'react-native-paper';
const df_avatar = require('../assets/default-avatar.jpg');

const Profile = (props) => {
    const [avatar, setAvatar] = useState();
    const [name, setName] = useState();
    const [open,setOpen] = useState(false)
    useEffect(() => {
        console.log(props?.login?.user?.data?.avatar);
        console.log(props?.login?.user?.data?.name);
        props?.login?.user?.data?.name ? setName(props?.login?.user?.data?.name) : setName('');
        props?.login?.user?.data?.avatar ? setAvatar(props?.login?.user?.data?.avatar) : setAvatar('');
    }, [props.login]);

    const ListImage = (image) => {
        if (!image) {
            return (
                <FastImage
                    style={styles.image}
                    source={df_avatar}
                    resizeMode={FastImage.resizeMode.cover}
                />
            );
        }
        return (
            <FastImage
                style={styles.image}
                source={{
                    uri: `${image}`,
                }}
                resizeMode={FastImage.resizeMode.cover}
            />
        );
    };
    const _onStateChange = ({ open }) => setOpen(open);
    const ActionSheetModal = () => (
        <FAB.Group
            open={open}
            icon={open ? 'calendar-today' : 'plus'}
            actions={[
                { icon: 'plus', onPress: () => console.log('Pressed add') },
                { icon: 'account-plus-outline', label: 'Chỉnh sửa thông tin', onPress: () => console.log('Pressed star')},
                { icon: 'bell', label: 'Thông báo', onPress: () => console.log('Pressed notifications') },
                { icon: 'email', label: 'Email', onPress: () => console.log('Pressed email') },

            ]}
            onStateChange={_onStateChange}
            onPress={() => {
                if (open) {
                    // do something if the speed dial is open
                }
            }}
        />
    );

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{paddingTop:36}}/>
                <View style={{alignSelf: 'center'}}>
                    <View style={styles.profileImage}>
                        {
                            ListImage(avatar)
                        }
                    </View>
                    <View style={styles.dm}>
                        <MaterialIcons name="chat" size={18} color="#DFD8C8"></MaterialIcons>
                    </View>
                    <View style={styles.active}></View>
                    <View style={styles.add}>
                        <Ionicons name="ios-add" size={48} color="#DFD8C8"
                                  style={{marginTop: 6, marginLeft: 2}}></Ionicons>
                    </View>
                </View>

                <View style={styles.infoContainer}>
                    <Text style={[styles.text, {fontWeight: '200', fontSize: 36}]}>{name}</Text>
                </View>

                <View style={styles.statsContainer}>
                    <View style={styles.statsBox}>
                        <Text style={[styles.text, {fontSize: 24}]}>483</Text>
                        <Text style={[styles.text, styles.subText]}>like</Text>
                    </View>
                    <View style={[styles.statsBox, {borderColor: '#DFD8C8', borderLeftWidth: 1, borderRightWidth: 1}]}>
                        <Text style={[styles.text, {fontSize: 24}]}>45,844</Text>
                        <Text style={[styles.text, styles.subText]}>Image</Text>
                    </View>
                    <View style={styles.statsBox}>
                        <Text style={[styles.text, {fontSize: 24}]}>302</Text>
                        <Text style={[styles.text, styles.subText]}>Following</Text>
                    </View>
                </View>

                <View style={{marginTop: 32}}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        {mocks.map((item, index) => {
                            return (

                                <View key={index} style={styles.mediaImageContainer}>
                                    <FastImage source={{uri: `${item.image}`}} style={styles.image}
                                               resizeMode={FastImage.resizeMode.cover}/>
                                </View>
                            );
                        })}

                    </ScrollView>

                </View>
                <Text style={[styles.subText, styles.recent]}>Recent Activity</Text>
                <View style={{alignItems: 'center'}}>
                    <View style={styles.recentItem}>
                        <View style={styles.activityIndicator}></View>
                        <View style={{width: 250}}>
                            <Text style={[styles.text, {color: '#41444B', fontWeight: '300'}]}>
                                Started following <Text style={{fontWeight: '400'}}>Jake Challeahe</Text> and <Text
                                style={{fontWeight: '400'}}>Luis Poteer</Text>
                            </Text>
                        </View>
                    </View>

                    <View style={styles.recentItem}>
                        <View style={styles.activityIndicator}></View>
                        <View style={{width: 250}}>
                            <Text style={[styles.text, {color: '#41444B', fontWeight: '300'}]}>
                                Started following <Text style={{fontWeight: '400'}}>Luke Harper</Text>
                            </Text>
                        </View>
                    </View>
                </View>

            </ScrollView>
            {
                ActionSheetModal()
            }
        </SafeAreaView>
    );
};
const mapStateToProps = state => {
    return {
        login: state.login,
    };
};

export default connect(mapStateToProps)(Profile);

